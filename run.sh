#!/bin/bash
CONTAINER_NAME=ci_cd_test
DOCKER_IMAGE='ci_cd_test:latest'

# Start Gunicorn processes
echo Starting Gunicorn.
function run {
  docker run -it \
    --name $CONTAINER_NAME \
    -p 8000:8000 \
    -d \
    $DOCKER_IMAGE
}

docker rm -f $CONTAINER_NAME
run
